package bff

import (
	"fastarmor/src/routes"
	"fastarmor/src/server"
	"os"
)

func Setup() {
	server.InitGlobalEchoHTTPServer()
	routes.Init()
	server.Echo.Logger.Fatal(server.Echo.Start(":" + os.Getenv("PORT")))
}
