package domain

import (
	"strconv"
)

type Attributes struct {
	Industry     string
	Organization string
	Country      string
	EventType    string
	EntityType   string
	EntityID     string
	EventID      string
	UserID       string
	Timestamp    int64
	UTCDate      string
	TraceID      string
}

type Event struct {
	Attributes
	Payload interface{}
}

func (e Event) GetAttributesAsMap() map[string]string {
	return map[string]string{
		"Industry":     e.Industry,
		"Organization": e.Organization,
		"Country":      e.Country,
		"EventType":    e.EventType,
		"EntityType":   e.EntityType,
		"EntityID":     e.EntityID,
		"EventID":      e.EventID,
		"UserID":       e.UserID,
		"Timestamp":    strconv.FormatInt(e.Timestamp, 10),
		"UTCDate":      e.UTCDate,
		"TraceID":      e.TraceID,
	}
}
