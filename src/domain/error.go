package domain

type Error struct {
	TraceID string
	Error   string
}
