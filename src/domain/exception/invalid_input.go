package exception

type InvalidInput struct {
	Message string
}

func (e InvalidInput) Error() string {
	if e.Message == "" {
		return "INVALID_INPUT"
	}
	return e.Message
}
