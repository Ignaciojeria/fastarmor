package constants

const JSON_INPUT_RECEIVED = "json input received"
const ERROR_UNMARSHALING_JSON_INPUT = "error unmarshaling json input"
const REQUIRED_ATTRIBUTES_MISSING = "required attributes missing"
const INPUT_VALIDATED_SUCCESSFULL = "input validated successfull"
const EVENT_ATTRIBUTES = "event_attributes"
const REQUEST_BODY = "request_body"
const TRACE_ID = "trace_id"
const SPAN_ID = "span_id"
