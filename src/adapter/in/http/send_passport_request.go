package http

import (
	"encoding/json"

	"fastarmor/src/adapter/out/commander"
	"fastarmor/src/constants"
	"fastarmor/src/domain"
	"fastarmor/src/domain/exception"
	"fastarmor/src/ports/out"
	"io"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// All command controllers inject entity publishers only
type sendPassportRequest struct {
	command out.Command
}

const send_passport_request_200 = "send_passport_request_200"
const send_passport_request_400 = "send_passport_request_400"
const send_passport_request_500 = "send_passport_request_500"

func SendPassportRequest() *sendPassportRequest {
	return &sendPassportRequest{
		command: commander.Command(),
	}
}

// Required attributes can change depending on usecase, try way to check required attributes dinamically
func (ctrl *sendPassportRequest) Handle(c echo.Context) error {

	span := trace.SpanFromContext(c.Request().Context())
	defer span.End()

	b, _ := io.ReadAll(c.Request().Body)
	span.AddEvent(constants.JSON_INPUT_RECEIVED,
		trace.WithAttributes(attribute.String(constants.REQUEST_BODY, string(b))))

	type SendPassportRequest struct {
		Email string
	}

	payload := new(SendPassportRequest)
	if err := json.Unmarshal(b, &payload); err != nil {
		span.SetAttributes(attribute.Bool("error", true))
		span.RecordError(err)
		span.AddEvent(constants.ERROR_UNMARSHALING_JSON_INPUT)
		return c.JSON(http.StatusInternalServerError, domain.Error{
			TraceID: span.SpanContext().TraceID().String(),
			Error:   err.Error(),
		})
	}

	if payload.Email == "" {
		span.SetAttributes(attribute.Bool("error", true))
		err := exception.InvalidInput{Message: "email is required"}
		span.RecordError(err)
		log.
			Error().
			Err(err).
			Str(constants.TRACE_ID, span.SpanContext().TraceID().String()).
			Str(constants.SPAN_ID, span.SpanContext().SpanID().String()).
			Interface(constants.REQUEST_BODY, payload).
			Msg(send_passport_request_400)
		span.AddEvent(constants.REQUIRED_ATTRIBUTES_MISSING)
		return c.JSON(http.StatusBadRequest, domain.Error{
			TraceID: span.SpanContext().TraceID().String(),
			Error:   err.Error(),
		})
	}
	span.AddEvent(constants.INPUT_VALIDATED_SUCCESSFULL)
	now := time.Now().UTC()
	event := domain.Event{
		Attributes: domain.Attributes{
			Industry:     "technology",
			Organization: "fastarmor",
			Country:      "cl",
			EventType:    "passportSended",
			EntityType:   "passport",
			EventID:      uuid.NewString(),
			Timestamp:    now.Unix(),
			UTCDate:      now.Format(time.RFC3339),
			EntityID:     payload.Email,
			TraceID:      span.SpanContext().TraceID().String(),
			UserID:       payload.Email,
		},
		Payload: payload,
	}
	if err := ctrl.command(c.Request().Context(), event); err != nil {
		log.
			Error().
			Err(err).
			Str(constants.TRACE_ID, span.SpanContext().TraceID().String()).
			Str(constants.SPAN_ID, span.SpanContext().SpanID().String()).
			Interface(constants.REQUEST_BODY, payload).
			Msg(send_passport_request_500)
		c.JSON(http.StatusInternalServerError, event.Attributes)
		return err
	}
	log.
		Info().
		Str(constants.TRACE_ID, span.SpanContext().TraceID().String()).
		Str(constants.SPAN_ID, span.SpanContext().SpanID().String()).
		Interface(constants.REQUEST_BODY, payload).
		Msg(send_passport_request_200)
	return c.JSON(http.StatusOK, event.Attributes)
}
