package commander

import (
	"context"
	"encoding/json"
	"fastarmor/src/domain"
	"fastarmor/src/infrastructure"
	"fastarmor/src/ports/out"
	"sync"

	"cloud.google.com/go/firestore"
	"cloud.google.com/go/pubsub"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type command struct {
	topic      *pubsub.Topic
	eventStore *firestore.Client
}

var firestore_tracer = otel.Tracer("firestore")
var pubsub_tracer = otel.Tracer("pubsub")

var once sync.Once

var commandOutPort out.Command

func Command() out.Command {
	once.Do(func() {
		p := &command{
			topic:      infrastructure.GetPubSubClient().Topic("fastarmor-bff-topic"),
			eventStore: infrastructure.GetFirestoreClient(),
		}
		commandOutPort = p.command
	})
	return commandOutPort
}

func (p *command) command(
	ctx context.Context,
	m domain.Event) error {

	ref := p.eventStore.
		Collection("bff-comands").
		Doc("commands").
		Collection(m.EventType).
		Doc(m.EntityID)

	_, firestoreSpan := firestore_tracer.Start(ctx, "save event transaction")
	defer firestoreSpan.End()
	err := p.eventStore.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		firestoreSpan.AddEvent("save & publish event transaction started",
			trace.WithAttributes(attribute.String("Country", m.Country)),
			trace.WithAttributes(attribute.String("EntityID", m.EntityID)),
			trace.WithAttributes(attribute.String("EventID", m.EventID)),
			trace.WithAttributes(attribute.String("EntityType", m.EntityType)),
			trace.WithAttributes(attribute.String("EventType", m.EventType)),
			trace.WithAttributes(attribute.String("Industry", m.Industry)),
			trace.WithAttributes(attribute.String("Organization", m.Organization)),
			trace.WithAttributes(attribute.String("UserID", m.UserID)),
		)
		if err2 := tx.Create(ref, m); err2 != nil {
			firestoreSpan.SetAttributes(attribute.Bool("error", true))
			firestoreSpan.AddEvent("error saving event transaction")
			return err2
		}
		b, _ := json.Marshal(m.Payload)
		pubsubContext, pubsubSpan := pubsub_tracer.Start(ctx, "publish event")
		defer pubsubSpan.End()
		if _, err := p.topic.Publish(pubsubContext, &pubsub.Message{
			Attributes: m.GetAttributesAsMap(),
			Data:       b,
		}).Get(pubsubContext); err != nil {
			pubsubSpan.SetAttributes(attribute.Bool("error", true))
			pubsubSpan.AddEvent("error publishing event. Transaction failed")
			return err
		}
		return nil
	})
	firestoreSpan.AddEvent("transaction finished ok")
	if err != nil {
		return err
	}
	return nil
}
