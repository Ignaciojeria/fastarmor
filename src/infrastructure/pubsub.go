package infrastructure

import (
	"context"
	"os"
	"sync"

	"cloud.google.com/go/pubsub"
	"github.com/rs/zerolog/log"
)

var pubsubClient *pubsub.Client

var once1 sync.Once

func GetPubSubClient() *pubsub.Client {
	once1.Do(func() {
		ctx := context.Background()
		client, err := pubsub.NewClient(ctx, os.Getenv("GCP_PROJECT_ID"))
		if err != nil {
			log.Error().Err(err).Send()
			os.Exit(0)
		}
		pubsubClient = client
	})
	return pubsubClient
}
