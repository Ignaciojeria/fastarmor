package infrastructure

import (
	"context"
	"os"
	"sync"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"github.com/rs/zerolog/log"
)

var firestoreClient *firestore.Client

var firebaseAuthClient *auth.Client

var firebaseApp *firebase.App

var once2 sync.Once

func getFirebaseAPP() *firebase.App {
	once2.Do(func() {
		// Use the application default credentials
		ctx := context.Background()
		conf := &firebase.Config{ProjectID: os.Getenv("GCP_PROJECT_ID")}
		app, err := firebase.NewApp(ctx, conf)
		firebaseApp = app
		if err != nil {
			log.Error().Err(err).Send()
			os.Exit(0)
		}
	})
	return firebaseApp
}

var once3 sync.Once

func GetFirestoreClient() *firestore.Client {
	once3.Do(func() {
		firestore, err := getFirebaseAPP().Firestore(context.Background())
		if err != nil {
			log.Error().Err(err).Send()
			os.Exit(0)
		}
		firestoreClient = firestore
	})
	return firestoreClient
}

var once4 sync.Once

func GetFirebaseAuthClient() *auth.Client {
	once4.Do(func() {
		client, err := getFirebaseAPP().Auth(context.Background())
		if err != nil {
			log.Error().Err(err).Send()
			os.Exit(0)
		}
		firebaseAuthClient = client
	})
	return firebaseAuthClient
}
