package routes

import (
	"fastarmor/src/adapter/in/http"
	"fastarmor/src/server"
)

func Init() {
	server.Echo.POST("/passports", http.SendPassportRequest().Handle)
}
