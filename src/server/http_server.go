package server

import (
	"sync"

	"github.com/labstack/echo/v4"
	"go.opentelemetry.io/contrib/instrumentation/github.com/labstack/echo/otelecho"
)

var Echo *echo.Echo

var once sync.Once

func InitGlobalEchoHTTPServer() *echo.Echo {
	once.Do(func() {
		Echo = echo.New()
		Echo.Use(otelecho.Middleware("fastarmor"))
	})
	return Echo
}
