package out

import (
	"context"
	"fastarmor/src/domain"
)

type Command func(context.Context, domain.Event) error
