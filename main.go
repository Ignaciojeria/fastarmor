package main

import (
	"context"
	bff "fastarmor/src"
	"os"
	"time"

	"github.com/joho/godotenv"
	zlg "github.com/mark-ignacio/zerolog-gcp"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
)

// tracerProvider returns an OpenTelemetry TracerProvider configured to use
// the Jaeger exporter that will send spans to the provided url. The returned
// TracerProvider will also use a Resource configured with all the information
// about the application.
func tracerProvider(url string) (*tracesdk.TracerProvider, error) {
	// Create the Jaeger exporter
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, err
	}
	tp := tracesdk.NewTracerProvider(
		// Always be sure to batch in production.
		tracesdk.WithBatcher(exp),
		// Record information about this application in a Resource.
		tracesdk.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("fastarmor-bff"),
			attribute.String("environment", os.Getenv("ENVIRONMENT")),
		)),
	)
	return tp, nil
}

func main() {
	if err := godotenv.Load(); err != nil {
		log.Error().Err(err).Send()
		os.Exit(0)
	}

	tp, err := tracerProvider(os.Getenv("OTEL_EXPORTER_JAEGER_ENDPOINT"))
	if err != nil {
		log.Error().Err(err).Send()
		os.Exit(0)
	}
	// Register our TracerProvider as the global so any imported
	// instrumentation in the future will default to using it.
	otel.SetTracerProvider(tp)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Cleanly shutdown and flush telemetry when the application exits.
	defer func(ctx context.Context) {
		// Do not make the application hang when it is shutdown.
		ctx, cancel = context.WithTimeout(ctx, time.Second*5)
		defer cancel()
		if err := tp.Shutdown(ctx); err != nil {
			log.Error().Err(err).Send()
			os.Exit(0)
		}
	}(ctx)
	initZeroLogStackDriverLogger(ctx)
	//server.InitGlobalEchoHTTPServer()
	bff.Setup()
	//server.Echo.Logger.Fatal(server.Echo.Start(":" + os.Getenv("PORT")))
}

func initZeroLogStackDriverLogger(ctx context.Context) {
	gcpWriter, err := zlg.NewCloudLoggingWriter(ctx,
		os.Getenv("GCP_PROJECT_ID"), "fastarmor-bff", zlg.CloudLoggingOptions{})
	if err != nil {
		log.Panic().Err(err).Msg("could not create a CloudLoggingWriter")
	}
	log.Logger = log.Output(gcpWriter)
}
